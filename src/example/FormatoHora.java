
package example;

public class FormatoHora {
    
    private int hora;
    private int minutos;

    FormatoHora(int hora) {
        this.hora = hora;
        this.minutos = 0;
    }
    
    FormatoHora(int hora, int minutos) {
        this.hora = hora;
        this.minutos = minutos;
    }
   
    private String formatMinutes(String minutos)
    {  
       if(minutos.length() == 1) {
           minutos = "0" + minutos;
           return minutos;
       } 
       
       return minutos; 
    }
    
    public String getHora() {
        String hora = Integer.toString(this.hora);
        String minutos = Integer.toString(this.minutos);

        String formato = hora + ":" + formatMinutes(minutos);
        return formato; 
    }
    
    public int getSoloHora() {
        return this.hora;
    }
    
    public int getSoloMinutos() {
        return this.minutos;
    }
}
