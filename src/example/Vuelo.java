
package example;

import java.util.ArrayList;

public class Vuelo {
    private final FormatoHora horaLLegada;
    private final FormatoHora horaSalida;
    private final int codigoVuelo;
    private final String aerolinea;
    private final String claseDeAvion;
    private final double valorPasaje;
    private ArrayList<Pasajero> pasajeros;

    public Vuelo(FormatoHora horaLLegada, 
            FormatoHora horaSalida, 
            int codigoVuelo, 
            String aerolinea, 
            String claseDeAvion,
            double valorPasaje,
            ArrayList<Pasajero> pasajeros) {
        
        this.horaLLegada = horaLLegada;
        this.horaSalida = horaSalida;
        this.codigoVuelo = codigoVuelo;
        this.aerolinea = aerolinea;
        this.claseDeAvion = claseDeAvion;
        this.valorPasaje = valorPasaje;
        this.pasajeros = pasajeros;
    }
    
    private int calcularDiferenciaDeTiempo(int llegada, int salida) {
        
        int diferenciaDeTiempo = llegada - salida;
        int unsignedDiferenciaTiempo = 
                (diferenciaDeTiempo < 0) ? 
                diferenciaDeTiempo * -1 : diferenciaDeTiempo;
        
        return unsignedDiferenciaTiempo;
    }
    
    public FormatoHora calcularDuracionDeVuelo() {
        
        
        int soloHoraLLegada = horaLLegada.getSoloHora();
        int soloMinutosLLegada = horaLLegada.getSoloMinutos();
        
        int soloHoraSalida = horaSalida.getSoloHora();
        int soloMinutosSalida = horaSalida.getSoloMinutos();
        
    
        int diferenciaDeHoras = 
            this.calcularDiferenciaDeTiempo(
                    soloHoraLLegada, 
                    soloHoraSalida);
        
        int diferenciaDeMinutos = 
            this.calcularDiferenciaDeTiempo(
                    soloMinutosSalida, 
                    soloMinutosLLegada);
        
        
        FormatoHora diferenciaEnHoras = 
                new FormatoHora(diferenciaDeHoras, diferenciaDeMinutos); 
        
        return diferenciaEnHoras;
    }
    
    public void mostrarDatosDePasajero(String pasajeroId) {
        
        for(Pasajero pasajero : this.pasajeros) { 
            
            if(pasajero.getNumeroID().equals(pasajeroId)) { 
                System.out.println(""
                        + "Nombre: " + pasajero.getNombres() + "\n"
                        + "Apellidos: " + pasajero.getApellidos() + "\n"
                        + "Tipo de ID: " + pasajero.getTipoID() + "\n"
                        + "Total a pagar (gratis si aplica descuento): " + pasajero.getTotalAPagar()
                        + ""); 
            }  
        }
    }
    
    public void aplicarDescuentos(int porcentaje) {
        for(Pasajero pasajero : this.pasajeros) { 
            if(pasajero.esFrecuente() == true) { 
                double valorPorcentaje = (this.valorPasaje * porcentaje) / 100;
                double totalConDescuento = this.valorPasaje - valorPorcentaje;
                pasajero.setTotalAPagar(totalConDescuento);
            } else {
                pasajero.setTotalAPagar(this.valorPasaje);
            }
         }
    }
}