
package example;

public class Pasajero {
    
    private String tipoID;
    private String numeroID;
    private String nombres;
    private String apellidos;
    private final boolean frecuente;
    private double totalAPagar;

    public Pasajero(
            String tipoID, 
            String numeroID, 
            String nombres, 
            String apellidos, 
            boolean frecuente) {
        
        this.tipoID = tipoID;
        this.numeroID = numeroID;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.frecuente = frecuente;
        
        this.totalAPagar = 0;
    }
    
    public double getTotalAPagar() {
        return totalAPagar;
    }

    public void setTotalAPagar(double totalAPagar) {
        this.totalAPagar = totalAPagar;
    }

    public boolean esFrecuente() {
        return frecuente;
    }

    public String getTipoID() {
        return tipoID;
    }

    public void setTipoID(String tipoID) {
        this.tipoID = tipoID;
    }

    public String getNumeroID() {
        return numeroID;
    }

    public void setNumeroID(String numeroID) {
        this.numeroID = numeroID;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    
}
