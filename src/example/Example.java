
package example;

import java.util.ArrayList;

/**
 *
 * @author duacos
 */
public class Example {
    
    public static void main(String[] args) {
        // para este ejercicio asumo que los vuelos salen y llegan el mismo dia xD
        // sale a la 1:20 pm y llega a las 8:30
        FormatoHora horaLlegada = new FormatoHora(13, 20);
        FormatoHora horaSalida = new FormatoHora(20, 30);
        
        Pasajero pas1 = new Pasajero(
                "cc", 
                "123456789", 
                "Jorge Iván", 
                "Durango Acosta",
                false);
        
        Pasajero pas2 = new Pasajero(
                "cc", 
                "123", 
                "Fulano", 
                "De tal",
                true);
        
        ArrayList<Pasajero> listaPasajeros = new ArrayList<Pasajero>();
        listaPasajeros.add(pas1);
        listaPasajeros.add(pas2);
        
        Vuelo colombiaMiami = new Vuelo(
                horaLlegada, 
                horaSalida, 
                234, 
                "American Airlines", 
                "Airbus Industrie A320",
                200.50,
                listaPasajeros
        );
        
        colombiaMiami.aplicarDescuentos(100);
        
        // 7 horas y 10 minutos
        System.out.println(colombiaMiami.calcularDuracionDeVuelo().getHora());
        
        System.out.println("---------------------------------------");
        colombiaMiami.mostrarDatosDePasajero("123"); // este usuario es frecuente por eso aplica descuento
        System.out.println("---------------------------------------");
        colombiaMiami.mostrarDatosDePasajero("123456789");
    }
}
